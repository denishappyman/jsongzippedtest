include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt
LIBS += -lz


HEADERS += \
        jsonsample.h \
        qcompressor/qcompressor.h \
        tst_case1.h


SOURCES += \
        main.cpp \
        qcompressor/qcompressor.cpp

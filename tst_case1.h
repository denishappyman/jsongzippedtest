#ifndef TST_CASE1_H
#define TST_CASE1_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <QString>
#include <QDebug>
#include <qcompressor/qcompressor.h>
#include <QTimer>
#include <future>
#include "jsonsample.h"

using namespace testing;

#define ASSERT_DURATION_LE(secs, stmt) { \
  std::promise<bool> completed; \
  auto stmt_future = completed.get_future(); \
  std::thread([&](std::promise<bool>& completed) { \
    stmt; \
    completed.set_value(true); \
  }, std::ref(completed)).detach(); \
  if(stmt_future.wait_for(std::chrono::seconds(secs)) == std::future_status::timeout) \
    GTEST_FATAL_FAILURE_("       timed out (> " #secs \
    " seconds). Check code for infinite loops"); \
}

#define MEASURE_DURATION(stmt) { \
typedef std::chrono::high_resolution_clock Time; \
typedef std::chrono::microseconds mks; \
auto t0 = Time::now(); \
stmt; \
auto t1 = Time::now(); \
mks d = std::chrono::duration_cast<mks>(t1 - t0); \
qDebug() << "Execution time: " << (d).count() << " mks"; \
}

class JsonLongQStringHolder : public ::testing::Test {
public:
  QString initialQSting;
  QByteArray compressed;
  QByteArray decompressed;
  QTimer timer;
  void SetUp()
  {
      initialQSting = jsonLongQString;
      qDebug() << "Initial plain text length is:" << initialQSting.length();
  }

  void TearDown()
  {
  }
};

class JsonShortQStringHolder : public ::testing::Test {
public:
  QString initialQSting;
  QByteArray compressed;
  QByteArray decompressed;
  QTimer timer;
  void SetUp()
  {
      initialQSting = jsonShortQString;
      qDebug() << "Initial plain text length is:" << initialQSting.length();
  }

  void TearDown()
  {
  }
};

TEST_F(JsonLongQStringHolder, DefaultCompressionMeasuringCase)
{
    // arrange
    int level = -1;
    bool success = false;
    auto&& input = initialQSting.toUtf8();
    // act
    MEASURE_DURATION(success = QCompressor::gzipCompress(input, compressed, level));
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();

    // assert
    ASSERT_FALSE(compressed.isEmpty());
}


TEST_F(JsonLongQStringHolder, DefaultDecompressionMeasuringCase)
{
    // arrange
    int level = -1;
    bool success = false;
    auto&& input = initialQSting.toUtf8();

    // act
    success = QCompressor::gzipCompress(input, compressed, level);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();

    MEASURE_DURATION(success = QCompressor::gzipDecompress(compressed, decompressed));
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    auto&& decompressedString = QString::fromLatin1(decompressed);

    // assert
    ASSERT_FALSE(compressed.isEmpty());
    ASSERT_EQ(initialQSting, decompressedString);
}

TEST_F(JsonLongQStringHolder, MaxCompressionDecompressionCase)
{
    // arrange
    int level = 9;
    bool success = false;
    auto&& input = initialQSting.toUtf8();

    // act
    success = QCompressor::gzipCompress(input, compressed, level);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();
    success = QCompressor::gzipDecompress(compressed, decompressed);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    auto&& decompressedString = QString::fromLatin1(decompressed);

    // assert
    ASSERT_FALSE(compressed.isEmpty());
    ASSERT_EQ(initialQSting, decompressedString);
}


TEST_F(JsonShortQStringHolder, DefaultCompressionMeasuringCase)
{
    // arrange
    int level = -1;
    bool success = false;
    auto&& input = initialQSting.toUtf8();
    // act
    MEASURE_DURATION(success = QCompressor::gzipCompress(input, compressed, level));
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();

    // assert
    ASSERT_FALSE(compressed.isEmpty());
}

TEST_F(JsonShortQStringHolder, DefaultDecompressionMeasuringCase)
{
    // arrange
    int level = -1;
    bool success = false;
    auto&& input = initialQSting.toUtf8();

    // act
    success = QCompressor::gzipCompress(input, compressed, level);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();

    MEASURE_DURATION(success = QCompressor::gzipDecompress(compressed, decompressed));
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    auto&& decompressedString = QString::fromLatin1(decompressed);

    // assert
    ASSERT_FALSE(compressed.isEmpty());
    ASSERT_EQ(initialQSting, decompressedString);
}

TEST_F(JsonShortQStringHolder, MaxCompressionDecompressionCase)
{
    // arrange
    int level = 9;
    bool success = false;
    auto&& input = initialQSting.toUtf8();

    // act
    success = QCompressor::gzipCompress(input, compressed, level);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    qDebug() << "Compressed text length is:" << compressed.length();
    success = QCompressor::gzipDecompress(compressed, decompressed);
    if(!success)
    {
        GTEST_FATAL_FAILURE_("Can't compress");
    }
    auto&& decompressedString = QString::fromLatin1(decompressed);

    // assert
    ASSERT_FALSE(compressed.isEmpty());
    ASSERT_EQ(initialQSting, decompressedString);
}

#endif // TST_CASE1_H
